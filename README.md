# Ngx Theme

Install the package

`npm i @microexcel-csd/ngx-theme`

## Setup

Add the ThemeModule to your app.module.ts (or whichever module will be using the themes)

```typescript
import { ThemeModule } from '@microexcel-csd/ngx-theme';
import { lightTheme, darkTheme } from './themes';

@NgModule({  
    declarations: [  
        AppComponent  
      ],  
      imports: [  
        BrowserModule,  
        ThemeModule.forRoot({  
          themes: [lightTheme, darkTheme],
          active: 'dark'  
        })  
      ],  
      providers: [],
      bootstrap: [AppComponent]
})
export class AppModule { }
```
            
Next you need to create your themes.

As you can see we import two themes from a themes.ts file, here is what our example file looks like:

```typescript
export const lightTheme = {
    name: 'light',
    properties: {
      '--background': '#EFF0F4',
      '--on-background': '#0F0F0F',
      '--primary': '#389BFF',
      '--on-primary': '#fff',
      '--secondary': '#B2B4B7',
      '--on-secondary': '#fff',
      '--surface': '#fff',
      '--on-surface': '#0F0F0F',
      '--error': '#EB5945',
      '--on-error': '#fff',
      '--warning': '#F6BB42',
      '--on-warning': '#fff'
    }
};
    
export const darkTheme = {
    name: 'dark',
    properties: {
    '--background': '#1F2125',
    '--on-background': '#fff',
    '--primary': '#2F90FF',
    '--on-primary': '#fff',
    '--secondary': '#474A4F',
    '--on-secondary': '#fff',
    '--surface': '#282A2F',
    '--on-surface': '#dddede',
    '--error': '#E74E3C',
    '--on-error': '#fff'
    }
};
```

You can create as many themes as you like with whatever variables that you would like, just make sure to add them all to the import in the NgModule

## Use

In order to apply a theme you must add the directive selector tag [ngx-theme] to an element in your application, all the children will also inherit the theme.

```html
<ul ngx-theme class="list">
  <li>
    <h2><a target="_blank" rel="noopener" (click)="toggle()">Tour of Heroes</a></h2>
  </li>
  <li>
    <h2><a target="_blank" rel="noopener" href="https://angular.io/cli">CLI Documentation</a></h2>
  </li>
  <li>
    <h2><a target="_blank" rel="noopener" href="https://blog.angular.io/">Angular blog</a></h2>
  </li>
</ul>
```
            
In order to apply the theme you will need to use the themes variables in your css. What we have done is apply the theme to the list class: 

```css
.list {
    background: var(--background);
    color: var(--primary);
    cursor: pointer;
}
```

Now in order to change your theme you will need to import the ThemeService into your component

```typescript

import { ThemeService } from '@microexcel-csd/ngx-theme';
```

Then you will inject it into your constructor. We have set up a simple toggle function to demonstrate changing themes

```typescript
  constructor(private themeService: ThemeService) {}

  toggle() {
    const active = this.themeService.getActiveTheme();
    console.log(active);
    if (active.name === 'light') {
      this.themeService.setTheme('dark');
    } else {
      this.themeService.setTheme('light');
    }
  }
```

And that's all to get started.

## API

| Service Function | Description | Parameters | Return |
| ------ | ------ | ------ | ------ |
| getTheme | Finds a theme from the imported list | name: string (Name of the theme to find) | Theme or Error: Not Found |
| getActiveTheme | Gets the current active theme | N/A | Theme |
| getProperty | Searches for the property in a theme | propName: string (Name of the property to find) | String of the property definition |
| setTheme | Sets the active theme | name: string (Name of the theme to set | N/A |
| registerTheme | Programatically add a new them to the array of themes | theme: Theme (Object matching the Theme type) | N/A |
| updateTheme | Update properties of a theme in the array of themes | name: string (Name of the theme), properties: {[key: string]: string} | N/A |


#### Support Our Packages
[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LM74WLHTJN8BA)
