import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ThemeModule } from 'projects/ngx-theme/src/public-api';

import { lightTheme, darkTheme } from './themes';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ThemeModule.forRoot({
      themes: [lightTheme, darkTheme],
      active: 'dark'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
