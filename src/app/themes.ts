export const lightTheme = {
    name: 'light',
    properties: {
      '--background': '#EFF0F4',
      '--on-background': '#0F0F0F',
      '--primary': '#389BFF',
      '--on-primary': '#fff',
      '--secondary': '#B2B4B7',
      '--on-secondary': '#fff',
      '--surface': '#fff',
      '--on-surface': '#0F0F0F',
      '--error': '#EB5945',
      '--on-error': '#fff',
      '--warning': '#F6BB42',
      '--on-warning': '#fff'
    }
  };



export const darkTheme = {
  name: 'dark',
  properties: {
    '--background': '#1F2125',
    '--on-background': '#fff',
    '--primary': '#2F90FF',
    '--on-primary': '#fff',
    '--secondary': '#474A4F',
    '--on-secondary': '#fff',
    '--surface': '#282A2F',
    '--on-surface': '#dddede',
    '--error': '#E74E3C',
    '--on-error': '#fff'
  }
};
