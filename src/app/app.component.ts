import { Component } from '@angular/core';
import { ThemeService } from 'projects/ngx-theme/src/public-api';

@Component({
  selector: 'thm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AngularTheme';

  constructor(private themeService: ThemeService) {}

  toggle() {
    const active = this.themeService.getActiveTheme();
    console.log(active);
    if (active.name === 'light') {
      this.themeService.setTheme('dark');
    } else {
      this.themeService.setTheme('light');
    }
  }

}
